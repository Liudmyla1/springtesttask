package com.example.testtask;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Random;

@Data
@Entity
public class Trapezium extends Figure{
    @Id
    @GeneratedValue
    private int idTrapezium;

    Trapezium(){
        type = "trapezium";
        Random rand = new Random();
        side1 = rand.nextInt(1000)+1;
        side2 = rand.nextInt(1000)+1;
        height = rand.nextInt(1000)+1;
    }
    public static int side1, side2, height;

    public static int getSide1() {
        return side1;
    }
    public static int getSide2() { return side2; }
    public static int getHeight() { return height; }

    public static double getArea() {
        return (side1+side2)*height/2;
    }

    void draw() {

    }
}
