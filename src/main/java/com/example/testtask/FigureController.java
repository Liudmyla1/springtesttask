package com.example.testtask;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

public class FigureController {
    static int k;
    //static Object[] figures = new Object[k];
    static DBConnector connection = new DBConnector();

    public static void genFigures(int amount) {
        k = amount;
        try (Statement statement = connection.getConnection().createStatement()) {
            //res = statement.executeQuery("");
            for (int i = 0; i < amount; i++) {
                Random rand = new Random();
                int randFigure = rand.nextInt(4) + 1;
                switch (randFigure) {
                    case 1:
                        Circle newFigure1 = new Circle();
                        newFigure1.setColor();
                        statement.addBatch("insert into Figure (type, colorName, area) values (circle, " + newFigure1.getColor() + ", " + newFigure1.getArea() + ")");
                        statement.addBatch("insert into Cirle (radius) value (" + newFigure1.getRadius() + ")");
                        break;
                    case 2:
                        Square newFigure2 = new Square();
                        newFigure2.setColor();
                        statement.addBatch("insert into Figure (type, colorName, area) values (square, " + newFigure2.getColor() + ", " + newFigure2.getArea() + ")");
                        statement.addBatch("insert into Square (side) value (" + newFigure2.getSide() + ")");
                        break;
                    case 3:
                        Trapezium newFigure3 = new Trapezium();
                        newFigure3.setColor();
                        statement.addBatch("insert into Figure (type, colorName, area) values (trapezium, " + newFigure3.getColor() + ", " + newFigure3.getArea() + ")");
                        statement.addBatch("insert into Trapezium (side1, side2, height) value (" + newFigure3.getSide1() + ", " + newFigure3.getSide2() + ", " + newFigure3.getHeight() + ")");
                        break;
                    case 4:
                        Triangle newFigure4 = new Triangle();
                        newFigure4.setColor();
                        statement.addBatch("insert into Figure (type, colorName, area) values (triangle, " + newFigure4.getColor() + ", " + newFigure4.getArea() + ")");
                        statement.addBatch("insert into Triangle (tSide1, tSide2, hypotenuse) value (" + newFigure4.getTSide1() + ", " + newFigure4.getTSide2() + ", " + newFigure4.getHypotenuse() + ")");
                        break;
                }
            }
            statement.executeBatch();
            statement.clearBatch();
            //res.close();
        } catch (SQLException e1) {
            e1.printStackTrace();
        }


    }

    public static void printAllFigures(){
        try (Statement statement = connection.getConnection().createStatement()) {
            ResultSet res = statement.executeQuery("select * from Figure");
            while (res.next()) {
                switch (res.getString(2)) {
                    case "circle":
                        ResultSet res2 = statement.executeQuery("select * from Circle");
                        System.out.println("id = " + res.getInt(1) + ", type = " + res.getString(2) +
                                ", radius = " + res2.getInt(2) + ", area = " + res.getDouble(4) +
                                ", color = " + res.getString(3));
                        break;
                    case "square":
                        res2 = statement.executeQuery("select * from Square");
                        System.out.println("id = " + res.getInt(1) + ", type = " + res.getString(2) +
                                ", side length = " + res2.getInt(2) + ", area = " + res.getDouble(4) +
                                ", color = " + res.getString(3));
                        break;
                    case "trapezium":
                        res2 = statement.executeQuery("select * from Trapezium");
                        System.out.println("id = " + res.getInt(1) + ", type = " + res.getString(2) +
                                ", side length = " + res2.getInt(2) + " and " + res2.getInt(3) +
                                ", height = " + res2.getInt(4) + ", area = " + res.getDouble(4) +
                                ", color = " + res.getString(3));
                        break;
                    case "triangle":
                        res2 = statement.executeQuery("select * from Triangle");
                        System.out.println("id = " + res.getInt(1) + ", type = " + res.getString(2) +
                                ", side length = " + res2.getInt(2) + " and " + res2.getInt(3) +
                                ", hypotenuse = " + res2.getDouble(4) + ", area = " + res.getDouble(4) +
                                ", color = " + res.getString(3));
                        break;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}


/*import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FigureController {

    private final FigureRepository repository;

    FigureController(FigureRepository repository) {
        this.repository = repository;
    }

    // Aggregate root

    @GetMapping("/figures")
    List<Figure> all() {
        return repository.findAll();
    }

    @PostMapping("/figures")
    Figure newFigure(@RequestBody Figure newFigure) {
        return repository.save(newFigure);
    }

    // Single item

    @GetMapping("/figures/{id}")
    Figure one(@PathVariable Long id) {

        return repository.findById(id)
                .orElseThrow(() -> new FigureNotFoundException(id));
    }

    @PutMapping("/figures/{id}")
    Figure replaceFigure(@RequestBody Figure newFigure, @PathVariable Long id) {

        return repository.findById(id)
                .map(figure -> {
                    figure.setType(newFigure.getType());
                    figure.setColor(newFigure.getColor());
                    figure.setColorName(newFigure.getColorName());
                    return repository.save(figure);
                })
                .orElseGet(() -> {
                    newFigure.setId(id);
                    return repository.save(newFigure);
                });
    }

    @DeleteMapping("/figures/{id}")
    void deleteFigure(@PathVariable Long id) {
        repository.deleteById(id);
    }*/

