package com.example.testtask;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Random;

@Data
@Entity
public class Square extends Figure {
    @Id
    @GeneratedValue
    private int idSquare;

    Square() {
        type = "square";
        Random rand = new Random();
        side = rand.nextInt(1000)+1;
    }
    public static int side;

    public static int getSide() {
        return side;
    }

    public static double getArea() {
        return side*side;
    }

    void draw() {

    }
}
