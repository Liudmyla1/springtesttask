package com.example.testtask;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Random;

@Data
@Entity
public class Circle extends Figure{

    @Id
    @GeneratedValue
    private int idCircle;

    Circle() {
        type = "circle";
        Random rand = new Random();
        radius = rand.nextInt(1000)+1;
    }
    public static int radius;

    public static int getRadius() {
        return radius;
    }

    public static double getArea() {
        return (radius*radius*Math.PI);
    }

    void draw() {

    }
}
