package com.example.testtask;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Random;

@Data
@Entity
public class Triangle extends Figure{
    @Id
    @GeneratedValue
    private int idTriangle;

    Triangle(){
        type = "triangle";
        Random rand = new Random();
        tSide1 = rand.nextInt(1000)+1;
        tSide2 = rand.nextInt(1000)+1;
    }
    public static int tSide1, tSide2;
    public static double hypotenuse;

    public static double getTSide1() { return tSide1; }
    public static double getTSide2() { return tSide2; }

    public  static Double getHypotenuse() {
        hypotenuse = Math.sqrt(tSide1*tSide1+tSide2*tSide2);
        return hypotenuse;
    }

    public static double getArea() {
        return tSide1*tSide2/2;
    }


    void draw() {

    }
}
