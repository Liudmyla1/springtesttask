package com.example.testtask;

import org.springframework.web.bind.annotation.*;

import static com.example.testtask.FigureController.*;

@RestController
@RequestMapping("/")
public class IndexController {
    //@RequestMapping(value = "/", method = RequestMethod.POST)
    @PostMapping("/")
    public int getAmount(@RequestParam("amount") int amount){
        System.out.println("Set amount of figures to generate");
        genFigures(amount);
        return amount;
    }

    @GetMapping("/figures")
    public void showDB() {
        printAllFigures();
    }
}
